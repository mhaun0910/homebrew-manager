#!/bin/bash
# curl -sSL https://gitlab.com/HomebrewSoft/homebrew-manager/-/raw/master/agent.bash | bash

apt update -y
apt install -y docker.io
apt install -y docker-compose

systemctl enable docker
systemctl start docker

docker run -d -p 9001:9001 --name portainer_agent --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/docker/volumes:/var/lib/docker/volumes portainer/agent

mkdir -p /root/dockers/nginx
curl https://gitlab.com/HomebrewSoft/homebrew-manager/-/raw/master/nginx-proxy-le.yml -o /root/dockers/nginx/docker-compose.yml
docker network create nginx-proxy
docker-compose -f /root/dockers/nginx/docker-compose.yml up -d

mkdir -p /root/dockers/local
mkdir -p /root/dockers/local/addons
mkdir -p /root/dockers/local/config
curl https://gitlab.com/HomebrewSoft/homebrew-manager/-/raw/master/odoo.conf -o /root/dockers/local/config/odoo.conf
curl https://gitlab.com/HomebrewSoft/homebrew-manager/-/raw/master/odoo-postgres-local.yml -o /root/dockers/local/docker-compose.yml
sed -i "s/\${ODOO_VERSION}/13.0/g" /root/dockers/local/docker-compose.yml
sed -i "s/\${POSTGRES_VERSION}/11.5/g" /root/dockers/local/docker-compose.yml
sed -i "s/\${DOMAIN}/test20m05d31v6.homebrewsoft.dev/g" /root/dockers/local/docker-compose.yml
sed -i "s/\${CONTACT}/moises@homebrewsoft.dev/g" /root/dockers/local/docker-compose.yml
docker-compose -f /root/dockers/local/docker-compose.yml up -d
