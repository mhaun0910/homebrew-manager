apt update -y
apt install docker.io -y
systemctl enable docker
systemctl start docker
docker network create nginx-proxy
docker run -d \
    --name=portainer \
    --restart=always \
    --network nginx-proxy \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /home/moy/dockers/portainer_data:/data \
    -e VIRTUAL_HOST=manager.homebrewsoft.dev \
    -e VIRTUAL_PORT=9000 \
    -e LETSENCRYPT_HOST=manager.homebrewsoft.dev \
    -e LETSENCRYPT_EMAIL=moises@homebrewsoft.dev \
    portainer/portainer
# TODO PASSWORD in portainer
# TODO SSH timeout
